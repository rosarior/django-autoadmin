from __future__ import unicode_literals

__author__ = 'Roberto Rosario'
__build__ = 0x010101
__copyright__ = 'Copyright 2014 Roberto Rosario'
__license__ = 'MIT'
__title__ = 'django-autoadmin'
__version__ = '1.1.1'

default_app_config = 'autoadmin.apps.AutoAdminAppConfig'
